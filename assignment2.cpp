#include <iostream>
#include <vector>

int produceFibRecursive(int n)
{
	if (n <= 0)
		return 0;

	if (n == 2 || n == 1)
		return 1;

	else
		return produceFib(n-1) + produceFib(n-2);
}

int findMaxPose(int * arr, size_t n)
{
	int currentPose = 0;
	int maxPose = 0;
	int j = 0;
	while (true)
	{
		for (j = currentPose + 1; j < n; j++)
		{
			if (arr[j] >= arr[maxPose])
			{
				maxPose = j;
				currentPose = j;
				break;
			}
		}
		if (j == n)
			break;
	}
	return maxPose;
}



std::vector<int> newFib(int n)
{
	std::vector<int> fibNumbers;
	fibNumbers.push_back(0);
	fibNumbers.push_back(1);


	for (int i = 2; i<n; i++)
	{
		fibNumbers.push_back(fibNumbers[i-1] + fibNumbers[i-2]);
	}

	return fibNumbers;
}

int main ()
{

	//definitions
	int n;
	std::cin >> n;

	std::vector<int> v = newFib(n);

	for (int j =0; j<n;j++)
	{
		std::cout << v[j] << " ";
	}


	return 0;
}
